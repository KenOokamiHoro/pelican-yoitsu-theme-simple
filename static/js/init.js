$(document).ready(function(){
    $(".button-collapse").sideNav();
    $('.dropdown-button').dropdown({
         inDuration: 300,
         outDuration: 225,
         constrain_width: false, // Does not change width of dropdown to that of the activator
         hover: false, // Activate on hover
         gutter: 1, // Spacing from edge
         belowOrigin: true, // Displays dropdown below the button
         alignment: 'right' // Displays dropdown with edge aligned to the left of button
       }
     );
    $('.collapsible').collapsible();
    $('.materialboxed').materialbox();
    $('.button-collapse-right').sideNav({
      menuWidth: 300, // Default is 240
      edge: 'right', // Choose the horizontal origin
      closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
      draggable: true // Choose whether you can drag to open on touch screens
    }
  );
  $('.modal').modal();
  $("nav").autoHidingNavbar();
  $('ul.tabs').tabs();
  // initialize v2mm comments
});